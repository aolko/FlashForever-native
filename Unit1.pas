{$R 'flash_ocx.res'}

unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  FlashPlayerControl, System.ImageList, Vcl.ImgList, Vcl.Menus, ABOUT,
  System.Actions, Vcl.ActnList, AdvPanel, Vcl.ComCtrls, AdvProgr, AdvTrackBar, ShellAPI,
  Vcl.Buttons;

type
  TForm1 = class(TForm)
    btnEdit: TButtonedEdit;
    ImageList1: TImageList;
    OpenDialogSWF: TOpenDialog;
    flash_player1: TFlashPlayerControl;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    View1: TMenuItem;
    Help1: TMenuItem;
    Quality1: TMenuItem;
    High1: TMenuItem;
    Medium1: TMenuItem;
    Low1: TMenuItem;
    Fullscreen1: TMenuItem;
    About1: TMenuItem;
    N1: TMenuItem;
    Open1: TMenuItem;
    Exit1: TMenuItem;
    ActionList1: TActionList;
    lbl1: TLabel;
    tmr1: TTimer;
    AdvPanel1: TAdvPanel;
    AdvTrackBar1: TAdvTrackBar;
    btn1: TSpeedButton;
    procedure btnEditRightButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure High1Click(Sender: TObject);
    procedure Medium1Click(Sender: TObject);
    procedure Low1Click(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure flash_player1ReadyStateChange(ASender: TObject;
      newState: Integer);
    procedure AdvTrackBar1EndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure AdvTrackBar1StartDrag(Sender: TObject;
      var DragObject: TDragObject);
    procedure AdvTrackBar1Change(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure WMDropFiles(var Msg: TMessage); message WM_DROPFILES;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  FlashCodeStream: TResourceStream;
  Version: string;
  SWFFrames: Integer;
  SWFCurFrame:Integer;

implementation

{$R *.dfm}


procedure TForm1.About1Click(Sender: TObject);
begin
     AboutBox.Show;
end;

procedure TForm1.AdvTrackBar1Change(Sender: TObject);
begin
  if AdvTrackBar1.Position <> flash_player1.CurrentFrame then begin
     flash_player1.FrameNum := AdvTrackBar1.Position;
     flash_player1.Play;
  end;
end;

procedure TForm1.AdvTrackBar1EndDrag(Sender, Target: TObject; X, Y: Integer);
begin
     //if flash_player1.Playing = false then
     //flash_player1.Play;
end;

procedure TForm1.AdvTrackBar1StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
     //if AdvTrackBar1.Position <> flash_player1.CurrentFrame then
     //flash_player1.StopPlay;
     //flash_player1.FrameNum := AdvTrackBar1.Position;
end;

procedure TForm1.btn1Click(Sender: TObject);
begin
     if btn1.Down = True then begin
         flash_player1.Loop := True;
     end else begin
         //btn1.Down := False;
         flash_player1.Loop := False;
     end;
end;

procedure TForm1.btnEditRightButtonClick(Sender: TObject);
begin
     if OpenDialogSWF.Execute then
     begin
          btnEdit.Text := OpenDialogSWF.FileName;
          flash_player1.Movie := OpenDialogSWF.FileName;
          flash_player1.Play;
     end;
end;

procedure TForm1.Exit1Click(Sender: TObject);
begin
     Application.Terminate;
end;

procedure TForm1.flash_player1ReadyStateChange(ASender: TObject;
  newState: Integer);
begin
     if newState = 3 then begin
        Form1.Caption := 'FlashForever - ' + string(ExtractFileName(OpenDialogSWF.FileName));
     end
     else if newState = 4 then begin
        tmr1.Enabled := True;
     end else begin
         ShowMessage(IntToStr(newState));
     end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
     Form1.Caption := 'FlashForever ' + Version;
     DragAcceptFiles(Self.Handle, True);
     if btn1.Down = True then begin
         flash_player1.Loop := True;
     end else begin
         //btn1.Down := False;
         flash_player1.Loop := False;
     end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
     DragAcceptFiles(Self.Handle, False);
end;

procedure TForm1.High1Click(Sender: TObject);
begin
     flash_player1.Quality := 1;
end;


procedure TForm1.Low1Click(Sender: TObject);
begin
     flash_player1.Quality := 0;
end;

procedure TForm1.Medium1Click(Sender: TObject);
begin
     flash_player1.Quality := 2;
end;

procedure TForm1.Open1Click(Sender: TObject);
begin
     if OpenDialogSWF.Execute then
     begin
          btnEdit.Text := OpenDialogSWF.FileName;
          flash_player1.Movie := OpenDialogSWF.FileName;
          flash_player1.BackgroundColor := 000000;
          flash_player1.Play;
     end;
end;

procedure TForm1.tmr1Timer(Sender: TObject);
begin
     if tmr1.Enabled = true then begin
     lbl1.Caption := IntToStr(flash_player1.CurrentFrame)+'/'+IntToStr(flash_player1.TotalFrames);
     AdvTrackBar1.Max := flash_player1.TotalFrames;
     AdvTrackBar1.Position := flash_player1.CurrentFrame;
     end;
end;

procedure TForm1.WMDropFiles(var Msg: TMessage);
var
  hDrop: THandle;
  FileCount: Integer;
  NameLen: Integer;
  I: Integer;
  S: string;

begin
  hDrop:= Msg.wParam;
  FileCount:= DragQueryFile (hDrop , $FFFFFFFF, nil, 0);

  for I:= 0 to FileCount - 1 do begin
    NameLen:= DragQueryFile(hDrop, I, nil, 0) + 1;
    SetLength(S, NameLen);
    DragQueryFile(hDrop, I, Pointer(S), NameLen);
    Form1.Caption := 'FlashForever - ' + string(ExtractFileName(S));
    flash_player1.Movie := S;
  end;

  DragFinish(hDrop);
end;

initialization

  FlashCodeStream := TResourceStream.Create(0, 'FlashOCXCode', 'BIN');
  FlashPlayerControl.LoadFlashOCXCodeFromStream(FlashCodeStream);
  FlashCodeStream.Free;
  Version := 'Beta';

end.
